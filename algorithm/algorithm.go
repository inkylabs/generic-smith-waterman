package algorithm

import (
	"fmt"
	"slices"
)

const GAP_PENALTY = -2
const MATCH_SCORING_FACTOR = 1

type DataAccessor interface {
	Size() (int, int)
	Distance(int, int) float32
}

func floatMatrix(dx int, dy int) [][]float32 {
	a := make([][]float32, dy)
	for i := range a {
		a[i] = make([]float32, dx)
	}
	return a
}

func stringMatrix(dx int, dy int) [][]string {
	a := make([][]string, dy)
	for i := range a {
		a[i] = make([]string, dx)
	}
	return a
}

func SW_Matrix(dac DataAccessor) [][]float32 {
	sizeX, sizeY := dac.Size()
	scoreMat := floatMatrix(sizeX+1, sizeY+1)
	vectorMat := stringMatrix(sizeX+1, sizeY+1)

	var maxVal float32 = 0
	var maxInds [][]int

	for row := range scoreMat {
		if row == 0 {
			continue
		}

		for col := range scoreMat[0] {
			if col == 0 {
				continue
			}

			dist := dac.Distance(col-1, row-1)

			topVal := scoreMat[row-1][col]
			leftVal := scoreMat[row][col-1]
			diagVal := scoreMat[row-1][col-1]

			topVal += GAP_PENALTY
			leftVal += GAP_PENALTY
			diagVal += (dist - 0.5) * -2

			var resVal float32 = 0
			var resVec string = ""

			if topVal > resVal {
				resVal = topVal
				resVec = "|"
			} else if leftVal > resVal {
				resVal = leftVal
				resVec = "-"
			} else if diagVal > resVal {
				resVal = diagVal
				resVec = "\\"
			} else {
				resVec = "0"
			}

			scoreMat[row][col] = resVal
			vectorMat[row][col] = resVec

			cellRef := []int{row, col}

			if resVal > maxVal {
				maxInds = [][]int{cellRef}
				maxVal = resVal
			} else if resVal == maxVal && resVal != 0 {
				maxInds = append(maxInds, cellRef)
			}
		}
	}

	alignment := getLocalAlignment(scoreMat, vectorMat, maxInds)
	fmt.Printf("alignment: %+v\n", alignment)

	return scoreMat
}

func printMatrixString(mat [][]string) {
	for row := range mat {
		for col := range mat[row] {
			fmt.Printf("%v ", mat[row][col])
		}
		fmt.Print("\n")
	}
}

func getLocalAlignment(scoreMat [][]float32, vectorMat [][]string, maxInds [][]int) [][][]int {
	fmt.Println("Vectors:")
	printMatrixString(vectorMat)

	var alignments [][][]int = [][][]int{}

	for mI := range maxInds {
		cellRef := maxInds[mI]

		var trace [][]int = [][]int{cellRef}

		for cellRef[0] > 0 && cellRef[1] > 0 {
			var nextCellRef []int
			vec := vectorMat[cellRef[0]][cellRef[1]]

			if vec == "|" {
				nextCellRef = []int{cellRef[0] - 1, cellRef[1]}
			} else if vec == "-" {
				nextCellRef = []int{cellRef[0], cellRef[1] - 1}
			} else if vec == "\\" {
				nextCellRef = []int{cellRef[0] - 1, cellRef[1] - 1}
			} else {
				break
			}

			fmt.Printf("Taking %v from %v to %v; trace so far is now %v\n", vec, scoreMat[cellRef[0]][cellRef[1]], scoreMat[nextCellRef[0]][nextCellRef[1]], trace)

			trace = append(trace, nextCellRef)
			cellRef = nextCellRef
		}

		fmt.Printf("Finished trace %v\n...from maxima %v\n", trace, maxInds[mI])
		slices.Reverse(trace)
		alignments = append(alignments, trace)
	}

	return alignments
}
