package algorithm

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

type da_implementing_dummy struct {
	seq1 string
	seq2 string
}

func (dad da_implementing_dummy) Size() (int, int) {
	return len(dad.seq1), len(dad.seq2)
}

func (dad da_implementing_dummy) Distance(x int, y int) float32 {
	if dad.seq1[x] == dad.seq2[y] {
		return 0
	} else {
		return 1
	}
}

func TestSW_MatrixDad(t *testing.T) {
	tests := map[string]struct {
		a        string
		b        string
		expected [][]float32
	}{
		"Square {ATCTG, ATGTG}": {
			a: "ATCTG",
			b: "ATGTG",
			expected: [][]float32{
				{0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000},
				{0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000},
				{0.000000, 0.000000, 2.000000, 0.000000, 1.000000, 0.000000},
				{0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 2.000000},
				{0.000000, 0.000000, 1.000000, 0.000000, 2.000000, 0.000000},
				{0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 3.000000},
			},
		},
		"Rectangle {ATCTG, ATCTBEAN}": {
			a: "ATCTG",
			b: "ATCTBEAN",
			expected: [][]float32{
				{0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000},
				{0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000},
				{0.000000, 0.000000, 2.000000, 0.000000, 1.000000, 0.000000},
				{0.000000, 0.000000, 0.000000, 3.000000, 1.000000, 0.000000},
				{0.000000, 0.000000, 1.000000, 1.000000, 4.000000, 2.000000},
				{0.000000, 0.000000, 0.000000, 0.000000, 2.000000, 3.000000},
				{0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000},
				{0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000},
				{0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			dad := da_implementing_dummy{seq1: test.a, seq2: test.b}

			got := SW_Matrix(dad)
			diff := cmp.Diff(test.expected, got)

			if diff != "" {
				t.Fatalf("SW_Matrix{%s, %s} mismatch from solution (-want +got):\n%s", test.a, test.b, diff)
			}
		})
	}
}
